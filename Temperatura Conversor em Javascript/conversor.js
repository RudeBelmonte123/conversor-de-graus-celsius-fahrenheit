function calcular(celsiusTxt){
    
    if(document.getElementById("celsiusTxt").value <= 0 || document.getElementById("celsiusTxt").value == null)
    {
        alert("ERROR COD 1: Não há valores para calcular!")
    }
        else{
            var x = document.getElementById("celsiusTxt").value;
            document.getElementById("resultado").innerHTML = (x * 9/5) + 32;
        }  
    }  
}

function limpar(celsiusTxt, resultado){

    if(document.getElementById("celsiusTxt").value >= 1){
        document.getElementById("celsiusTxt").value = null;
        document.getElementById("resultado").innerHTML = null;
    }else{
        alert("ERROR COD 2: Não há nada para limpar!")
    }

}